<?php

namespace backend\modules\subjects\models;

use Yii;

/**
 * This is the model class for table "{{%subjects}}".
 *
 * @property integer $id
 * @property string $nametj
 * @property string $nameru
 * @property integer $type1
 * @property integer $type2
 * @property integer $type3
 * @property integer $lang 
 *
 * @property Questions1[] $questions1s
 * @property Questions2[] $questions2s
 * @property Questions3[] $questions3s
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subjects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nametj', 'nameru','type1', 'type2', 'type3','lang'], 'required'],
            [['type1', 'type2', 'type3'], 'integer'],
            [['nametj', 'nameru'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nametj' => 'Тоҷики',
            'nameru' => 'Русский',
            'type1' => 'мик-сав-нав1',
            'type2' => 'мик-сав-нав2',
            'type3' => 'мик-сав-нав3',
            'lang' => Yii::t('app', 'Саволҳои ин фанн дар ҳарду забон якхелаанд?'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions1s()
    {
        return $this->hasMany(Questions1::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions2s()
    {
        return $this->hasMany(Questions2::className(), ['subject_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions3s()
    {
        return $this->hasMany(Questions3::className(), ['subject_id' => 'id']);
    }
}