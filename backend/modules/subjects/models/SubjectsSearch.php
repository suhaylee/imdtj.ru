<?php

namespace backend\modules\subjects\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\subjects\models\Subjects;

/**
 * SubjectsSearch represents the model behind the search form about `backend\modules\subjects\models\Subjects`.
 */
class SubjectsSearch extends Subjects
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type1', 'type2', 'type3'], 'integer'],
            [['nametj', 'nameru'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Subjects::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type1' => $this->type1,
            'type2' => $this->type2,
            'type3' => $this->type3,
            'lang' => $this->lang, 
        ]);

        $query->andFilterWhere(['like', 'nametj', $this->nametj])
            ->andFilterWhere(['like', 'nameru', $this->nameru]);

        return $dataProvider;
    }
}
