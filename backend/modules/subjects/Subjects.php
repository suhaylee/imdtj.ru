<?php

namespace backend\modules\subjects;

class Subjects extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\subjects\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
