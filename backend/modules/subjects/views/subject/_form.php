<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\subjects\models\Subjects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="subjects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nametj')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nameru')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type1')->textInput() ?>

    <?= $form->field($model, 'type2')->textInput() ?>

    <?= $form->field($model, 'type3')->textInput() ?>
    
    <?= $form->field($model, 'lang')->dropDownList(['0'=>'Не','1'=>'Ҳа']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Илова кардан') : Yii::t('app', 'Ислоҳ кардан'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
