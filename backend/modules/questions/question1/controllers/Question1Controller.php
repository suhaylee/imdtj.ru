<?php

namespace backend\modules\questions\question1\controllers;

use Yii;
use backend\modules\questions\question1\models\Questions1;
use backend\modules\questions\question1\models\Question1Search;

use vova07\imperavi\actions\GetAction;
use vova07\imperavi\actions\UploadAction;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;
/**
 * Question1Controller implements the CRUD actions for Questions1 model.
 */
class Question1Controller extends Controller
{
	public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }
	
	/*public function actions()
	{
	   return [
		   'error' => [
			   'class' => 'yii\web\ErrorAction',
		   ],
		'image-upload' => [
				   'class' => 'vova07\imperavi\actions\UploadAction',
				   'url' => '/images/blog/', // Directory URL address, where files are stored.
				   'path' => '@webroot/images/blog/' // Or absolute path to directory where files are stored.
			   ],
		'images-get' => [
				   'class' => 'vova07\imperavi\actions\GetAction',
				   'url' => '/images/blog/', // Directory URL address, where files are stored.
				   'path' => '@webroot/images/blog/', // Or absolute path to directory where files are stored.
				   'type' => '0',
			   ],
		'files-get' => [
				   'class' => 'vova07\imperavi\actions\GetAction',
				   'url' => '/files/blog/', // Directory URL address, where files are stored.
				   'path' => '@webroot/files/blog/', // Or absolute path to directory where files are stored.
				   'type' => '1',//GetAction::TYPE_FILES,
			   ],
		'file-upload' => [
				   'class' => 'vova07\imperavi\actions\UploadAction',
				   'url' => '/files/blog/', // Directory URL address, where files are stored.
				   'path' => '@webroot/files/blog/' // Or absolute path to directory where files are stored.
			   ],
		   ];
	}*/
	
	public function actions()
	{
		return [
			'image-upload' => [
				'class' => 'vova07\imperavi\actions\UploadAction',
				'url' => $this->module->contentUrl, // Directory URL address, where files are stored.
				'path' => $this->module->contentPath // Or absolute path to directory where files are stored.
			],
			'images-get' => [
				'class' => 'vova07\imperavi\actions\GetAction',
				'url' => $this->module->contentUrl, // Directory URL address, where files are stored.
				'path' => $this->module->contentPath, // Or absolute path to directory where files are stored.
				'type' => GetAction::TYPE_IMAGES,
			],
			'file-upload' => [
				'class' => 'vova07\imperavi\actions\UploadAction',
				'url' => $this->module->contentUrl, // Directory URL address, where files are stored.
				'path' => $this->module->contentUrl, // Or absolute path to directory where files are stored.
				'uploadOnlyImage' => false, // For not image-only uploading.
			],
		];
	}

    /**
     * Lists all Questions1 models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Question1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Questions1 model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Questions1 model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Questions1();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Questions1 model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Questions1 model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Questions1 model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Questions1 the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Questions1::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
