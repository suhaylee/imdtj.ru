<?php

namespace backend\modules\questions\question1;

class question1 extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\questions\question1\controllers';
	
	public $contentUrl = '/statics/blogs/content';
	public $contentPath = '@statics/web/blogs/content';
	public $fileUrl = '/statics/blogs/files';
	public $filePath = '@statics/web/blogs/files';
	public $imagesTempPath = '@statics/temp/blogs/images/';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
