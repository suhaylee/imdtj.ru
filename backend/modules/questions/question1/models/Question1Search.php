<?php

namespace backend\modules\questions\question1\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\questions\question1\models\Questions1;


use backend\modules\subjects\models\Subjects;

/**
 * Question1Search represents the model behind the search form about `backend\modules\questions\question1\models\Questions1`.
 */
class Question1Search extends Questions1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ans'], 'integer'],
            [['lang', 'subject_id', 'text', 'var1', 'var2', 'var3', 'var4'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questions1::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ans' => $this->ans,
        ]);

		$query->joinWith('subjects');

        $query->andFilterWhere(['like', 'lang', $this->lang])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'var1', $this->var1])
            ->andFilterWhere(['like', 'var2', $this->var2])
            ->andFilterWhere(['like', 'var3', $this->var3])
            ->andFilterWhere(['like', 'var4', $this->var4])
            ->andFilterWhere(['like', 'Imd_Subjects.nametj', $this->subject_id]);

        return $dataProvider;
    }
}
