<?php

namespace backend\modules\questions\question1\models;

use Yii;

use backend\modules\subjects\models\Subjects;

/**
 * This is the model class for table "imd_questions1".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property string $lang
 * @property string $text
 * @property string $var1
 * @property string $var2
 * @property string $var3
 * @property string $var4
 * @property integer $ans
 *
 * @property Subjects $subject
 * @property Subjects $subjects
 */
class Questions1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'imd_questions1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'lang', 'ans'], 'required'],
            [['subject_id', 'ans'], 'integer'],
            [['text', 'var1', 'var2', 'var3', 'var4'], 'string'],
            [['lang'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject_id' => Yii::t('app', 'Фанн'),
            'lang' => Yii::t('app', 'Забон'),
            'text' => Yii::t('app', 'Тексти Савол'),
            'var1' => Yii::t('app', 'Варианти А'),
            'var2' => Yii::t('app', 'Варианти B'),
            'var3' => Yii::t('app', 'Варианти С'),
            'var4' => Yii::t('app', 'Варианти D'),
            'ans' => Yii::t('app', 'Варианти Дуруст'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    /*public function getSubject()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'subject_id']);
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjects()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'subject_id']);
    }
}
