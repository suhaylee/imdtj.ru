<?php

use yii\helpers\Html;
use yii\grid\GridView;


use backend\modules\subjects\models\Subjects;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\questions\question1\models\Question1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\ArrayHelper;

$this->title = Yii::t('app', 'Саволҳои навъи якум');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions1-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Илова кардан'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

	<? 
		$a = array(5);
		$a[1] = 'A';
		$a[2] = 'B';
		$a[3] = 'C';
		$a[4] = 'D';
	?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
				'attribute'=>'subject_id',
				'value'=>'subjects.nametj',
				'filter'=>ArrayHelper::map(Subjects::find()->asArray()->all(), 'nametj', 'nametj')
			],
			[
				'attribute'=>'lang',
				'value'=>'lang',
				'filter'=>array('Тоҷикӣ'=>'Тоҷикӣ','Русский'=>'Русский')
			],
            'text:ntext',
            //'var1:ntext',
            //'var2:ntext',
            //'var3:ntext',
            //'var4:ntext',
             'ans',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
