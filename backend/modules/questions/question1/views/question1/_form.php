<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use yii\helpers\ArrayHelper;
use backend\modules\subjects\models\Subjects;

//use dosamigos\ckeditor\CKEditor;

use vova07\imperavi\Widget;
use yii\helpers\Url;

use yii\web\JsExpression;

use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question1\models\Questions1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions1-form">

    <?php $form = ActiveForm::begin(); ?>
	
    <?= $form->field($model, 'subject_id')->dropDownList(
		ArrayHelper::map(Subjects::find()->all(),'id','nametj'),
		['prompt'=>'Фанни саволро интихоб кунед!']
	) ?>
	
    <?= $form->field($model, 'lang')->dropDownList(['Тоҷикӣ'=>'Тоҷикӣ','Русский'=>'Русский'],['prompt'=>'Забони саволро интихоб кунед!']) ?>
    
	<?= $form->field($model, 'text')->textArea() ?>
    
	<script>
		var cktextarea_history = CKEDITOR.replace( 'Questions1[text]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history});
	</script>
    
    
    <?= $form->field($model, 'var1')->textArea() ?>
    
	<script>
		var cktextarea_history1 = CKEDITOR.replace( 'Questions1[var1]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history1});
	</script>
    
    <?= $form->field($model, 'var2')->textArea() ?>
    
	<script>
		var cktextarea_history11 = CKEDITOR.replace( 'Questions1[var2]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history11});
	</script>
    
    <?= $form->field($model, 'var3')->textArea() ?>
    
	<script>
		var cktextarea_history111 = CKEDITOR.replace( 'Questions1[var3]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history111});
	</script>
    
    <?= $form->field($model, 'var4')->textArea() ?>
    
	<script>
		var cktextarea_history1111 = CKEDITOR.replace( 'Questions1[var4]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history1111});
	</script>
	
	<?= $form->field($model, 'ans')->dropDownList(['1'=>'A','2'=>'B','3'=>'C','4'=>'D'],['prompt'=>'Варианти дурустро интихоб кунед!']) ?>
	
    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Илова кардан') : Yii::t('app', 'Изменить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
