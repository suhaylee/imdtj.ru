<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question1\models\Questions1 */

$this->title = Yii::t('app', 'Илова кардани саволи навъи якум');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions1s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
