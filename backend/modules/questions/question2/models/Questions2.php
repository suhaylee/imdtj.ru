<?php

namespace backend\modules\questions\question2\models;

use backend\modules\subjects\models\Subjects;

use Yii;

/**
 * This is the model class for table "{{%questions2}}".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property string $lang
 * @property string $text
 * @property string $var1
 * @property string $var2
 * @property string $var3
 * @property string $var4
 * @property string $choice1
 * @property string $choice2
 * @property string $choice3
 * @property string $choice4
 * @property string $choice5
 * @property integer $ansA
 * @property integer $ansB
 * @property integer $ansC
 * @property integer $ansD
 *
 * @property Subjects $subject
 */
class Questions2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questions2}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'lang', 'ansA', 'ansB', 'ansC', 'ansD'], 'required'],
            [['subject_id', 'ansA', 'ansB', 'ansC', 'ansD'], 'integer'],
            [['text', 'var1', 'var2', 'var3', 'var4', 'choice1', 'choice2', 'choice3', 'choice4', 'choice5'], 'string'],
            [['lang'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject_id' => Yii::t('app', 'Фанн'),
            'lang' => Yii::t('app', 'Забон'),
            'text' => Yii::t('app', 'Тексти Савол'),
            'var1' => Yii::t('app', 'Варианти А'),
            'var2' => Yii::t('app', 'Варианти B'),
            'var3' => Yii::t('app', 'Варианти C'),
            'var4' => Yii::t('app', 'Варианти D'),
            'choice1' => Yii::t('app', 'Интихоби 1'),
            'choice2' => Yii::t('app', 'Интихоби 2'),
            'choice3' => Yii::t('app', 'Интихоби 3'),
            'choice4' => Yii::t('app', 'Интихоби 4'),
            'choice5' => Yii::t('app', 'Интихоби 5'),
            'ansA' => Yii::t('app', 'Ҷавоби Варианти A'),
            'ansB' => Yii::t('app', 'Ҷавоби Варианти B'),
            'ansC' => Yii::t('app', 'Ҷавоби Варианти C'),
            'ansD' => Yii::t('app', 'Ҷавоби Варианти D'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'subject_id']);
    }
}
