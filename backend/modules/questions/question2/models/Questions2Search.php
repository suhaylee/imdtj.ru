<?php

namespace backend\modules\questions\question2\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\questions\question2\models\Questions2;

/**
 * Questions2Search represents the model behind the search form about `backend\modules\questions\question2\models\Questions2`.
 */
class Questions2Search extends Questions2
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'ansA', 'ansB', 'ansC', 'ansD'], 'integer'],
            [['lang', 'subject_id', 'text', 'var1', 'var2', 'var3', 'var4', 'choice1', 'choice2', 'choice3', 'choice4', 'choice5'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questions2::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ansA' => $this->ansA,
            'ansB' => $this->ansB,
            'ansC' => $this->ansC,
            'ansD' => $this->ansD,
        ]);

		$query->joinWith('subject');

        $query->andFilterWhere(['like', 'lang', $this->lang])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'var1', $this->var1])
            ->andFilterWhere(['like', 'var2', $this->var2])
            ->andFilterWhere(['like', 'var3', $this->var3])
            ->andFilterWhere(['like', 'var4', $this->var4])
            ->andFilterWhere(['like', 'choice1', $this->choice1])
            ->andFilterWhere(['like', 'choice2', $this->choice2])
            ->andFilterWhere(['like', 'choice3', $this->choice3])
            ->andFilterWhere(['like', 'choice4', $this->choice4])
            ->andFilterWhere(['like', 'choice5', $this->choice5])
            ->andFilterWhere(['like', 'Imd_Subjects.nametj', $this->subject_id]);

        return $dataProvider;
    }
}
