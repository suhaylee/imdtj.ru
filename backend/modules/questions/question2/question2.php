<?php

namespace backend\modules\questions\question2;

class question2 extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\questions\question2\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
