<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question2\models\Questions2 */

$this->title = Yii::t('app', 'Илова кардани саволи навъи дуюм');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions2s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
