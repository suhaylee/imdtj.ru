<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


use yii\helpers\ArrayHelper;
use backend\modules\subjects\models\Subjects;

/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question2\models\Questions2 */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions2s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions2-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'subject.nametj',
            'lang',
            'text:ntext',
            'var1:ntext',
            'var2:ntext',
            'var3:ntext',
            'var4:ntext',
            'choice1:ntext',
            'choice2:ntext',
            'choice3:ntext',
            'choice4:ntext',
            'choice5:ntext',
            'ansA',
            'ansB',
            'ansC',
            'ansD',
        ],
    ]) ?>

</div>
