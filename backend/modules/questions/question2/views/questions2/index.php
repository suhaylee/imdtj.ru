<?php

use yii\helpers\Html;
use yii\grid\GridView;


use yii\helpers\ArrayHelper;
use backend\modules\subjects\models\Subjects;


/* @var $this yii\web\View */
/* @var $searchModel backend\modules\questions\question2\models\Questions2Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Саволҳои навъи дуюм');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions2-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Илова кардан'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'subject_id',
			[
				'attribute'=>'subject_id',
				'value'=>'subject.nametj',
				'filter'=>ArrayHelper::map(Subjects::find()->asArray()->all(), 'nametj', 'nametj')
			],
            [
				'attribute'=>'lang',
				'value'=>'lang',
				'filter'=>array('Тоҷикӣ'=>'Тоҷикӣ','Русский'=>'Русский')
			],
            'text:ntext',
            //'var1:ntext',
            // 'var2:ntext',
            // 'var3:ntext',
            // 'var4:ntext',
            // 'choice1:ntext',
            // 'choice2:ntext',
            // 'choice3:ntext',
            // 'choice4:ntext',
            // 'choice5:ntext',
             'ansA',
             'ansB',
             'ansC',
             'ansD',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
