<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question2\models\Questions2Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions2-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'subject_id') ?>

    <?= $form->field($model, 'lang') ?>

    <?= $form->field($model, 'text') ?>

    <?= $form->field($model, 'var1') ?>

    <?php // echo $form->field($model, 'var2') ?>

    <?php // echo $form->field($model, 'var3') ?>

    <?php // echo $form->field($model, 'var4') ?>

    <?php // echo $form->field($model, 'choice1') ?>

    <?php // echo $form->field($model, 'choice2') ?>

    <?php // echo $form->field($model, 'choice3') ?>

    <?php // echo $form->field($model, 'choice4') ?>

    <?php // echo $form->field($model, 'choice5') ?>

    <?php // echo $form->field($model, 'ansA') ?>

    <?php // echo $form->field($model, 'ansB') ?>

    <?php // echo $form->field($model, 'ansC') ?>

    <?php // echo $form->field($model, 'ansD') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
