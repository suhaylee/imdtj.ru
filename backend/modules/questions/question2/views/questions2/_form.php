<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use yii\helpers\ArrayHelper;
use backend\modules\subjects\models\Subjects;

use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question2\models\Questions2 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions2-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'subject_id')->dropDownList(
		ArrayHelper::map(Subjects::find()->all(),'id','nametj'),
		['prompt'=>'Фанни саволро интихоб кунед!']
	) ?>
    
    <?= $form->field($model, 'lang')->dropDownList(['Тоҷикӣ'=>'Тоҷикӣ','Русский'=>'Русский'],['prompt'=>'Забони саволро интихоб кунед!']) ?>



    <?= $form->field($model, 'text')->textArea() ?>
    
	<script>
		var cktextarea_history = CKEDITOR.replace( 'Questions2[text]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history});
	</script>
    
    
    <?= $form->field($model, 'var1')->textArea() ?>
    
	<script>
		var cktextarea_history1 = CKEDITOR.replace( 'Questions2[var1]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history1});
	</script>
    
    <?= $form->field($model, 'var2')->textArea() ?>
    
	<script>
		var cktextarea_history11 = CKEDITOR.replace( 'Questions2[var2]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history11});
	</script>
    
    <?= $form->field($model, 'var3')->textArea() ?>
    
	<script>
		var cktextarea_history111 = CKEDITOR.replace( 'Questions2[var3]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history111});
	</script>
    
    <?= $form->field($model, 'var4')->textArea() ?>
    
	<script>
		var cktextarea_history1111 = CKEDITOR.replace( 'Questions2[var4]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history1111});
	</script>


     <?= $form->field($model, 'choice1')->textArea() ?>
    
	<script>
		var cktextarea_history2 = CKEDITOR.replace( 'Questions2[choice1]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history2});
	</script>
    
    <?= $form->field($model, 'choice2')->textArea() ?>
    
	<script>
		var cktextarea_history22 = CKEDITOR.replace( 'Questions2[choice2]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history22});
	</script>
    
    <?= $form->field($model, 'choice3')->textArea() ?>
    
	<script>
		var cktextarea_history222 = CKEDITOR.replace( 'Questions2[choice3]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history222});
	</script>
    
    <?= $form->field($model, 'choice4')->textArea() ?>
    
	<script>
		var cktextarea_history2222 = CKEDITOR.replace( 'Questions2[choice4]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history2222});
	</script>
    
    <?= $form->field($model, 'choice5')->textArea() ?>
    
	<script>
		var cktextarea_history22222 = CKEDITOR.replace( 'Questions2[choice5]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history22222});
	</script>
    

    <?= $form->field($model, 'ansA')->dropDownList(['1'=>'Интихоби 1','2'=>'Интихоби 2','3'=>'Интихоби 3','4'=>'Интихоби 4','5'=>'Интихоби 5'],['prompt'=>'Дурусташро интихоб кунед!']) ?>
	
    <?= $form->field($model, 'ansB')->dropDownList(['1'=>'Интихоби 1','2'=>'Интихоби 2','3'=>'Интихоби 3','4'=>'Интихоби 4','5'=>'Интихоби 5'],['prompt'=>'Дурусташро интихоб кунед!']) ?>

    <?= $form->field($model, 'ansC')->dropDownList(['1'=>'Интихоби 1','2'=>'Интихоби 2','3'=>'Интихоби 3','4'=>'Интихоби 4','5'=>'Интихоби 5'],['prompt'=>'Дурусташро интихоб кунед!']) ?>

    <?= $form->field($model, 'ansD')->dropDownList(['1'=>'Интихоби 1','2'=>'Интихоби 2','3'=>'Интихоби 3','4'=>'Интихоби 4','5'=>'Интихоби 5'],['prompt'=>'Дурусташро интихоб кунед!']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Илова кардан') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
