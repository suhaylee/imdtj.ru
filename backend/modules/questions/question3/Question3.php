<?php

namespace backend\modules\questions\question3;

class Question3 extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\questions\question3\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
