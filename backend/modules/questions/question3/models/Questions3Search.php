<?php

namespace backend\modules\questions\question3\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\questions\question3\models\Questions3;

/**
 * Questions3Search represents the model behind the search form about `backend\modules\questions\question3\models\Questions3`.
 */
class Questions3Search extends Questions3
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['lang', 'text', 'ans', 'subject_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Questions3::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

		$query->joinWith('subject');
		
		
        $query->andFilterWhere(['like', 'lang', $this->lang])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'ans', $this->ans])
            ->andFilterWhere(['like', 'Imd_Subjects.nametj', $this->subject_id]);

        return $dataProvider;
    }
}
