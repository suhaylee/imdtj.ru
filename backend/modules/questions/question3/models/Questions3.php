<?php

namespace backend\modules\questions\question3\models;

use Yii;


use backend\modules\subjects\models\Subjects;


/**
 * This is the model class for table "{{%questions3}}".
 *
 * @property integer $id
 * @property integer $subject_id
 * @property string $lang
 * @property string $text
 * @property string $ans
 *
 * @property Subjects $subject
 */
class Questions3 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%questions3}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'lang', 'ans'], 'required'],
            [['subject_id'], 'integer'],
            [['text'], 'string'],
            [['lang'], 'string', 'max' => 25],
            [['ans'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'subject_id' => Yii::t('app', 'Subject ID'),
            'lang' => Yii::t('app', 'Lang'),
            'text' => Yii::t('app', 'Text'),
            'ans' => Yii::t('app', 'Ans'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'subject_id']);
    }
}
