<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question3\models\Questions3 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Questions3',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions3s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="questions3-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
