<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use yii\helpers\ArrayHelper;
use backend\modules\subjects\models\Subjects;

use dosamigos\ckeditor\CKEditor;


/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question3\models\Questions3 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="questions3-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'subject_id')->dropDownList(
		ArrayHelper::map(Subjects::find()->all(),'id','nametj'),
		['prompt'=>'Фанро интихоб кунед!']
	) ?>

  	<?= $form->field($model, 'lang')->dropDownList(['Тоҷикӣ'=>'Тоҷикӣ','Русский'=>'Русский'],['prompt'=>'Забонро интихоб кунед!']) ?>

	<?= $form->field($model, 'text')->textArea() ?>
    
	<script>
		var cktextarea_history = CKEDITOR.replace( 'Questions3[text]' );
		AjexFileManager.init({returnTo: 'ckeditor', editor: cktextarea_history});
	</script>

    <?= $form->field($model, 'ans')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
