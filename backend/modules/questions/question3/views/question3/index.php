<?php

use yii\helpers\Html;
use yii\grid\GridView;


use yii\helpers\ArrayHelper;
use backend\modules\subjects\models\Subjects;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\questions\question3\models\Questions3Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Questions3s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions3-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Questions3'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			[
				'attribute'=>'subject_id',
				'value'=>'subject.nametj',
				'filter'=>ArrayHelper::map(Subjects::find()->asArray()->all(), 'nametj', 'nametj')
			],
            [
				'attribute'=>'lang',
				'value'=>'lang',
				'filter'=>array('Tajik'=>'Tajik','Russian'=>'Russian')
			],
            'text:ntext',
            //'ans',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
