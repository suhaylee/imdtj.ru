<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\modules\questions\question3\models\Questions3 */

$this->title = Yii::t('app', 'Create Questions3');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Questions3s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="questions3-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
